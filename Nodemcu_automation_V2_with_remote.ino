#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRutils.h>
#define BLYNK_PRINT Serial    
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <EEPROM.h>

const uint16_t kRecvPin = D4;
IRrecv irrecv(kRecvPin);
decode_results results;

int EEaddress1 = 0;       ////for save the last data
int EEaddress2 = 2;
int EEaddress3 = 4;
int EEaddress4 = 6;
int EEaddress5 = 8;
int EEaddress6 = 10;
int EEaddress7 = 12;
int EEaddress8 = 14;
int light_state_1;
int light_state_2;
int light_state_3;
int light_state_4;
int manual_switch_state_1;
int manual_switch_state_2;
int manual_switch_state_3;
int manual_switch_state_4;
int previous_manual_switch_state_1;
int previous_manual_switch_state_2;
int previous_manual_switch_state_3;
int previous_manual_switch_state_4;
int rx_Pin = 03;                    //RX PIN ASSIGN FOR OUTPUT
int count =0;
byte pre_manual_state1;           //for save switch data
byte pre_manual_state2;
byte pre_manual_state3;
byte pre_manual_state4;
byte pre_light_state1;        //for save previous light state
byte pre_light_state2;
byte pre_light_state3;
byte pre_light_state4; 
long unsigned remote_value_1;
long unsigned remote_value_2;
long unsigned remote_value_3;
long unsigned remote_value_4;

void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn();
  EEPROM.begin(512);
  EEPROM.get(EEaddress1,pre_manual_state1);
  EEPROM.get(EEaddress2,pre_manual_state2);
  EEPROM.get(EEaddress3,pre_manual_state3);
  EEPROM.get(EEaddress4,pre_manual_state4);
  EEPROM.get(EEaddress5,pre_light_state1);
  EEPROM.get(EEaddress6,pre_light_state2);
  EEPROM.get(EEaddress7,pre_light_state3);
  EEPROM.get(EEaddress8,pre_light_state4);
  previous_manual_switch_state_1 = pre_manual_state1;
  previous_manual_switch_state_2 = pre_manual_state2;
  previous_manual_switch_state_3 = pre_manual_state3;
  previous_manual_switch_state_4 = pre_manual_state4;
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(rx_Pin, OUTPUT);
  pinMode(D2, INPUT);
  pinMode(D5, INPUT);
  pinMode(D6, INPUT);
  pinMode(D7, INPUT);
  digitalWrite(D0, pre_light_state1);
  digitalWrite(D1, pre_light_state2);
  digitalWrite(D3, pre_light_state3);
  digitalWrite(rx_Pin, pre_light_state4);
  WiFi.begin("Raj", "raj@00321");
}
 
BLYNK_CONNECTED() {
  Blynk.virtualWrite(V1, light_state_1);
  Blynk.virtualWrite(V2, light_state_2);
  Blynk.virtualWrite(V3, light_state_3);
  Blynk.virtualWrite(V4, light_state_4);
}

BLYNK_WRITE(V1) {
  light_state_1 = param.asInt();
  digitalWrite(D0, light_state_1);
}
BLYNK_WRITE(V2) {
  light_state_2 = param.asInt();
  digitalWrite(D1, light_state_2);
}
BLYNK_WRITE(V3) {
  light_state_3 = param.asInt();
  digitalWrite(D3, light_state_3);
}
BLYNK_WRITE(V4) {
  light_state_4 = param.asInt();
  digitalWrite(rx_Pin, light_state_4);
}

void loop(){
  Blynk.run();
  if (Blynk.connected()==false){
    count=count+1;
    Serial.println(count);
  }
  if (count==1050){
//    Blynk.config("Htr8gKfrdj0bDbVJjlyQDSeFMNI7RKoc");             ////BEDROOM
    Blynk.config("rpcSOFs40OnyjQ_Cn96Tc_nj3x7DRguY");            //// My Room
    count=0;
  }
  light_state_1=digitalRead(D0);
  light_state_2=digitalRead(D1);
  light_state_3 = digitalRead(D3);
  light_state_4 = digitalRead(rx_Pin);
  manual_switch_state_1=digitalRead(D2);
  manual_switch_state_2=digitalRead(D5);
  manual_switch_state_3 = digitalRead(D6);
  manual_switch_state_4 = digitalRead(D7);
  if (irrecv.decode(&results)!=0 || manual_switch_state_1 != previous_manual_switch_state_1) {
    remote_value_1 = results.value;
    Serial.println(remote_value_1);
    if(manual_switch_state_1 != previous_manual_switch_state_1 || remote_value_1 == 2155840125){     //// 33444015 bedroom
      if(light_state_1 == LOW){
        digitalWrite(D0,HIGH);
        Blynk.virtualWrite(V1, HIGH);
      }
      else {
        digitalWrite(D0,LOW);
        Blynk.virtualWrite(V1, LOW);
      }
    }
  }
     
  if(irrecv.decode(&results)!=0 || manual_switch_state_2 != previous_manual_switch_state_2){
    remote_value_2 = results.value;
    if(manual_switch_state_2 != previous_manual_switch_state_2 || remote_value_2 == 2155831965){     //// 33478695 bedroom
      if(light_state_2 == LOW){
        digitalWrite(D1,HIGH);
        Blynk.virtualWrite(V2, HIGH);
      }
      else {
        digitalWrite(D1,LOW);
        Blynk.virtualWrite(V2, LOW);
      }
    }
  }  

  if(irrecv.decode(&results)!=0 || manual_switch_state_3 != previous_manual_switch_state_3){  
    remote_value_3 = results.value;
    if (manual_switch_state_3 != previous_manual_switch_state_3 || remote_value_3 == 2155848285) {     //// 33486855 bedroom
      if (light_state_3 == LOW) {
        digitalWrite(D3, HIGH);
        Blynk.virtualWrite(V3, HIGH);
      }
      else {
        digitalWrite(D3, LOW);
        Blynk.virtualWrite(V3, LOW);
      }
    }
  }

  if(irrecv.decode(&results)!=0 || manual_switch_state_4 != previous_manual_switch_state_4){
    remote_value_4 = results.value;
    if (manual_switch_state_4 != previous_manual_switch_state_4 || remote_value_4 == 2155815645) {    //// 33486855 bedroom
      if (light_state_4 == LOW) {
        digitalWrite(rx_Pin, HIGH);
        Blynk.virtualWrite(V4, HIGH);
      }
      else {
        digitalWrite(rx_Pin, LOW);
        Blynk.virtualWrite(V4, LOW);
      }
    }
    irrecv.resume();
  } 

  previous_manual_switch_state_1 = manual_switch_state_1;
  previous_manual_switch_state_2 = manual_switch_state_2;
  previous_manual_switch_state_3 = manual_switch_state_3;
  previous_manual_switch_state_4 = manual_switch_state_4;
  EEPROM.commit();
  EEPROM.write(EEaddress1,manual_switch_state_1);
  EEPROM.write(EEaddress2,manual_switch_state_2);
  EEPROM.write(EEaddress3,manual_switch_state_3);
  EEPROM.write(EEaddress4,manual_switch_state_4);
  EEPROM.write(EEaddress5,light_state_1);
  EEPROM.write(EEaddress6,light_state_2);
  EEPROM.write(EEaddress7,light_state_3);
  EEPROM.write(EEaddress8,light_state_4);
}
